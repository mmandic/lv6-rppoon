using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_rppoon
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
