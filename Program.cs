using System;

namespace LV6_rppoon
{
    class Program
    {
        static void Main(string[] args)
        {
           //ZAD1
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("firstNote", "Everything is gonna be alright tomorrow!"));
            notebook.AddNote(new Note("secondNote", "My precious"));
            notebook.AddNote(new Note("thirdNote", "You smile,I smile!"));

            IAbstractIterator notebookIterator = notebook.GetIterator();

            while (!notebookIterator.IsDone)
            {
                notebookIterator.Current.Show();
                notebookIterator.Next();
            }
            //ZAD2
            Box box = new Box();
            box.AddProduct(new Product("laptop ACER", 1000));
            box.AddProduct(new Product("mouse", 5));
            IAbstractIterator boxIterator = box.GetIterator();
            while (!boxIterator.IsDone)
            {
                Console.WriteLine(boxIterator.Current.ToString());
                boxIterator.Next();
            }
            
        }
    }
}
